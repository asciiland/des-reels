require "rake/testtask"
require "./draft.rb"

Rake::TestTask.new(:test) do |t|
  # t.libs << "test"
  # t.libs << "lib"
  t.test_files = FileList["test/**/test_*.rb"]
end

Rake::TaskManager.record_task_metadata = true

def task_title(t)
  [ ' ',
    '#' * 80,
    " [#{t.name_with_args}] #{Time.now}",
    " #{t.comment}",
    '#' * 80 ] * "\n"
end


namespace :render do
  namespace :beta do
    desc "Render beta frame v1"
    task :one do
      breath_string = ' .., \':'
      heart_string = ' oaVx   e/ /    dX   d    '
      frame = EDC::Reals::Frame.new(breath_string, heart_string, 290, 210)

      puts "BREATH >>>"
      puts frame.breath.make_it_sound(frame.columns, frame.rows)
      puts "HEART >>>"
      puts frame.heart.make_it_sound(frame.columns, frame.rows)
      puts "NERVES >>>"
      puts frame.nerves.make_it_sound(frame.columns, frame.rows)
    end
    desc "Render beta frame v2"
    task :two do
      breath_string = ' .., \':'
      heart_string = ' oaVx   e/ /    dX   d    '
      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)

      puts "BREATH >>>"
      puts frame.stretch_to_frame(breath_string, frame.columns, frame.rows).join("\n")
      puts "HEART >>>"
      puts frame.stretch_to_frame(heart_string,frame.columns, frame.rows).join("\n")
      puts "NERVES >>>"
      puts frame.nerves.make_it_sound(frame.columns, frame.rows)
    end
    desc "Render beta frame v3"
    task :three do
      breath_string = ' .., \':'
      heart_string = ' - | -_    //   '
      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)

      puts "BREATH >>>"
      puts frame.stretch_to_frame(breath_string, frame.columns, frame.rows).join("\n")
      puts "HEART >>>"
      puts frame.stretch_to_frame(heart_string,frame.columns, frame.rows).join("\n")
      puts "NERVES >>>"
      puts frame.nerves.make_it_sound(frame.columns, frame.rows)
    end
    desc "Render beta frame v4"
    task :four do
      breath_string = ' .., .,.. .,,; .,,.        '
      heart_string = ' o oo ooo oO0o0o'
      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)

      puts "BREATH >>>"
      puts frame.stretch_to_frame(breath_string, frame.columns, frame.rows).join("\n")
      puts "HEART >>>"
      puts frame.stretch_to_frame(heart_string,frame.columns, frame.rows).join("\n")
      puts "NERVES >>>"
      puts frame.nerves.make_it_sound(frame.columns, frame.rows, 0.1)
    end
  end

  namespace :'41-se-faire-virer' do
    desc "Render frame 'Se faire virer' (def)"
    task :def do
      breath_string = ' .., \':'
      heart_string = ' oaVx   e/ /    dX   d    '
      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)

      puts "BREATH >>>"
      puts frame.stretch_to_frame(breath_string, frame.columns, frame.rows).join("\n")
      puts "HEART >>>"
      puts frame.stretch_to_frame(heart_string,frame.columns, frame.rows).join("\n")
      puts "NERVES >>>"
      puts frame.nerves.make_it_sound(frame.columns, frame.rows)
    end
  end

  namespace :'42-la-melancolie-de-la-nasse' do
    desc "Render frame 'La mélancolie de la nasse' (v1)"
    task :one do
      # C'est plutôt un souffle long, avec des variations comme des hoquets
      breath_string = '~~~~~&~~~~~~~~~~~~~^~~~~~~~    '

      # Le cœur bat régulièrement.
      # Le narrateur/protagoniste vit plusieurs épisodes dans ce court récit, 
      # il participe à une manifestation où il se sent poète aventurier, 
      # une sorte de lyrisme romanesque se dégage de son style, 
      # il imagine des lendemains qui chantent, des révolutions qui aboutissent, etc. 
      # Il est ensuite pris dans une nasse, mais bien qu'il arrive à faire ressentir l'aspect angoissant d'une nasse, 
      # il semble finalement assez détaché. \
      # Il est enfin arrêté et subit un interrogatoire burlesque ou il mène en bourrique celle qui l'interroge. 
      # Il joue le simple d'esprit, celui qui ne comprend pas. 
      # On l'imagine bien mou, mais solide pourtant dans sa position puisque nous savons le jeu qu'il joue.
      # Le cœur bat donc avec une certaine régularité, même si, certainement, 
      # il ne s'agit que du rythme cardiaque qui transparait à travers la peau pour ceux qui le regardent.
      #
      heart_string = ' · · · '

      # Les nerfs : quelle densité sur une échelle de 1 à 10 ? 
      # (plus c'est dense, plus il y aura de caractères sur la couv)
      #
      # Très difficile à dire, je dirais 6
      tense = 0.6

      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)

      puts "BREATH >>>"
      puts frame.stretch_to_frame(breath_string, frame.columns, frame.rows) * ''
      puts "HEART >>>"
      puts frame.stretch_to_frame(heart_string,frame.columns, frame.rows) * ''
      puts "NERVES >>>"
      puts frame.nerves.make_it_sound(frame.columns, frame.rows, tense)
    end
    desc "Render frame 'La mélancolie de la nasse' (v2)"
    task :two do
      breath_string = '____—————----·••··---———_'
      heart_string = ' · 0 · ~ ··· o   '
      tense = 0.9

      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)

      puts "BREATH >>>"
      puts frame.stretch_to_frame(breath_string, frame.columns, frame.rows) * ''
      puts "HEART >>>"
      puts frame.stretch_to_frame(heart_string,frame.columns, frame.rows) * ''
      puts "NERVES >>>"
      puts frame.nerves.make_it_sound(frame.columns, frame.rows, tense)
    end
    desc "Render frame 'La mélancolie de la nasse' (v3)"
    task :three do
      breath_string = '____—————----·••··---———_'
      heart_string = ' po·m     pooom                           /śk               '
      tense = 0.9

      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)

      puts "BREATH >>>"
      puts frame.stretch_to_frame(breath_string, frame.columns, frame.rows) * ''
      puts "HEART >>>"
      puts frame.stretch_to_frame(heart_string,frame.columns, frame.rows) * ''
      puts "NERVES >>>"
      puts frame.nerves.make_it_sound(frame.columns, frame.rows, tense)
    end
    desc "Render frame 'La mélancolie de la nasse' (def). See `dist/42-melancolie-nasse-v2a.txt`"
    task :def do
      Rake::Task['render:42-la-melancolie-de-la-nasse:two'].invoke
    end
  end

  namespace :'32-les-routiers-du-vide' do
    desc "Render frame 'Les routiers du vide' (v1a)"
    task :one do
      # Le souffle:
      # On est sur un souffle court, haletant
      #
      breath_string = '....^....^^....^^^....    '
      # Le cœur:
      # La singularité de ce récit est qu'il est écrit à deux voix qui semblent
      # n'en former qu'une... pour autant les protagonistes sont bien distincts
      # et se distinguent l'un de l'autre. Le rythme s'en ressent, il
      # y a quelque chose de l'ordre de la régularité qui viendrait être
      # percuté de temps en temps par la pulsation soudaine, forte des deux
      # protagonistes, de leur verve,...
      heart_string = 'db db db X!y    ae ae ae {[<& ]//}   0 o 0 o  *(i"")'
      #
      # Les nerfs:
      # Texte nerveux, il y a comme une tension constante, les auteurs se
      # sentant perdu dans leur enquête, dans leur quête de sens
      # journalistique, dans leur vie aussi, mais cette tension donne au texte
      # une belle saveur.
      #
      tense = 0.9

      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)

      puts "BREATH >>>"
      puts frame.stretch_to_frame(breath_string, frame.columns, frame.rows) * ''
      puts "HEART >>>"
      puts frame.stretch_to_frame(heart_string,frame.columns, frame.rows) * ''
      puts "NERVES >>>"
      puts frame.nerves.make_it_sound(frame.columns, frame.rows, tense)
    end
    desc "Render frame 'Les routiers du vide' (v1b)"
    task :one_b do
      breath_string = '....^....^^....^^^....    '
      heart_string = 'db db db X!y    ae ae ae {[<& ]//}   0 o 0 o  *(i"")'
      tense = 0.9

      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180, tense)
      frame.render
    end
    desc "Render frame 'Les routiers du vide' (v2a)"
    task :two_a do
      breath_string = '..-^ ...--^ '
      heart_string = 'db    db    db    db    db    YxyX    ..    /%/|(g3wpi3457t90-p   > .   '
      tense = 0.9

      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180, tense)
      frame.render
    end
    desc "Render frame 'Les routiers du vide' (def). See `dist/32-les-routiers-du-vide-v2b.txt`"
    task :def do
      Rake::Task['render:32-les-routiers-du-vide:two_a'].invoke
    end
  end

  namespace :'36-insolations' do
    desc "Render frame 'Insolations' (v1)"
    task :one do |t|
      # Le souffle: clairement ample, c'est un texte qui a été écrit en
      # respirant, c'est même le texte qui ouvre la respiration (le texte est
      # ponctué de dates suivies de "je ne veux plus rien étouffer")
      breath_string = '_;il(())li;_    '
      # Le cœur: je dirais battements réguliers, avec quelques accélérations de
      # temps à autre, mais on reste sur un rythme poétique, régulier
      heart_string = ('DCDCDCDCDC o o o o ' * 7) << '^ ! > '
      # Les nerfs: sur une échelle de 1 à 10, je dirais du 9, on oscille autour
      # de la violence profonde, elle est toujours là, pas loin, on cohabite
      # avec elle et on le sait dès le début. On ressent qu'il s'agit de dire
      # des choses qui ont été tues trop longtemps, que c'est dur, mais que ça
      # a aussi besoin de prendre son temps.
      tense = 0.9
      # Extraits Insolations pour 4ème de couv’
      #
      # « L’Algérie transperce mon corps. Je la défends toujours contre ses
      # censeurs, même quand elle a tort. Je ne supporte pas que l’on puisse
      # l’inquiéter encore. Je suis loin du temps de la guerre. Je suis loin
      # des pluies et des volcans froids. Je n’ai pas connu la peau mendiante
      # des violentes offensives, le récit que l’on ne m’a jamais rapporté
      # transperce pourtant mon corps. Je suis héritière d’un tremblement de
      # terre, d’un soleil bouillant dans les veines, d’une espérance
      # maladroite. »
      #
      # « Je suis héritière d’une Algérie d’été, de ses plages bordées par la
      # montagne, du soleil brûlant qui pique sur la peau, de l’odeur de la
      # résine habitant la forêt. Je suis héritière de l’Algérie fragile, de
      # l’Algérie joyeuse, familiale, chantante. Cette Algérie est éphémère.
      # Juillet et août sont des mirages absorbant la misère des dix autres
      # mois de l’année. »
      #
      # « Prononcer me donne la nausée. Quand je prononce, je prends peur. Je
      # ne suis rien, rien de ce qui se prononce.
      # Je ne veux pas être et je ne serai pas.
      # Je veux juste aimer Alice et qu’on me foute la paix.
      # Je n’ai pas besoin d’avoir une étiquette pour que mon désir soit légitime. »
      #
      # « L’adulte ne savait pas pour les fils. Mais je lui en veux quand même.
      # Il savait ce que ça voulait dire, d’être une fille seule, là–bas, dans
      # un monde abîmé, dans un monde qui ne sait pas se réparer, un pays que
      # l’on a dépossédé. Forcément, si on vous dépossède, vous reprenez. Mais
      # vous reprenez tout, et parfois même ce qui ne vous appartient pas. »
      #
      # « Elle s’est accrochée au sol de mon corps, la terre de l’enfance et
      # puis elle s’est tassée dans mon cœur, ça pourrirait ainsi, pour la vie.
      # C’était à ma manière, à la manière des douleurs fières. »

      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)
      puts task_title(t)
      frame.render
    end
    desc "Render frame 'Insolations' (def). See `dist/36-insolations-v1a.txt`"
    task :def do
      Rake::Task['render:36-insolations:one'].invoke
    end
  end

  namespace :'nn-l-echarde' do
    desc "Render frame 'L\'écharde' (v1)"
    task :one do |t|
      # Le souffle
      # On est sur un souffle long, très long
      breath_string = '_~-{  '
      # Le cœur
      # Pulsation régulière
      heart_string = ('b p b p b p o o ' * 7) << '| '
      # Les nerfs
      # Une blessure, une tension qui reste, une écharde qui reste plantée, mais tout en maintenant une certaine douceur.
      tense = 0.6
      # Auto-fiction sur une histoire qui est arrivée à l'autrice sur une
      # emprise qu'elle a vécu alors qu'elle avait à peine 14 ans, avec un
      # adulte, son prof de théâtre.
      #
      # > Une écharde est un corps étranger inséré accidentellement dans la
      # peau. Ce livre est le récit d’une écharde dans l’intime, les éclats
      # d’un corps d’adulte dans la peau d’une adolescente. C’est son
      # professeur de théâtre. Elle est amoureuse. Elle veut croire à une vie
      # plus intense, c’est ce qu’il lui promet. Elle ment à ses parents, elle
      # s’isole de ses amis, elle se réfugie dans la danse, dans l’école, ses
      # nuits se peuplent d’ombres étranges, et le récit tient. Un jour, elle
      # y met fin. Et elle essaie de vivre, blessée par l’écharde, souffrant
      # sans en identifier la cause. Une écharde peut mettre des années
      # à tomber, elle fait son trajet sous la peau, diffuse, demeure. On
      # rencontre parfois un regard attentif qui la surprend et la reconnaît,
      # et des mains patientes pour la retirer. L’Echarde est le récit
      # intime et politique d’une société où certains hommes s’aveuglent sur la
      # nature du désir des jeunes filles ; ce livre vient de l’émancipation de
      # l’une d’entre elles.
      #

      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)
      puts task_title(t)
      frame.render
    end
    # desc "Render frame 'L\'écharde' (def). See `dist/nn-l-echarde-v1a.txt`"
    # task :def do
    #   Rake::Task['render:nn-l-echarde:one'].invoke
    # end
  end

  namespace :'nn-aux-tendresses' do
    desc "Render frame 'Aux tendresses, un corps (v1)"
    task :one do |t|
      # Le souffle
      breath_string = '~> . % . [] ... '
      # Le cœur
      heart_string = ('i   i   i   I   t   *  . .    ' * 5) << '+ '
      # Les nerfs
      tense = 0.6

      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)
      puts task_title(t)
      frame.render
    end
    # desc "Render frame 'Aux tendresses, un corps (def). See `dist/nn-aux-tendresses-v1a.txt`"
    # task :def do
    #   Rake::Task['render:nn-aux-tendresses:one'].invoke
    # end
  end

  namespace :'50-casser-du-sucre' do
    desc "Render frame 'Casser du sucre à la pioche et mourir au travail (v1)"
    task :one do |t|
      # Le souffle
      breath_string = '|—'
      # Le cœur
      heart_string = ('//////  >     ' * 5) << ''
      # Les nerfs
      tense = 1

      frame = EDC::Reals::Frame.new(breath_string, heart_string, 200, 180)
      puts task_title(t)
      frame.render
    end
    desc "Render frame 'Casser du sucre à la pioche et mourir au travail (def). See `dist/50-casser-du-sucre-v1a.txt`"
    task :def do
      Rake::Task['render:50-casser-du-sucre:one'].invoke
    end
  end

  desc "Render demo"
  task :demo do
    breath_string = ' .., \':'
    heart_string = ' oaVx   e/ /    dX   d    '
    frame = EDC::Reals::Frame.new(breath_string, heart_string, 290, 210)

    # puts ''
    # puts breath_string
    # puts ''
    # puts frame.breath.make_it_sound(frame.columns, frame.rows)
    # puts ''
    # puts heart_string
    # puts ''
    # puts frame.heart.make_it_sound(frame.columns, frame.rows)
    puts ''
    puts frame.nerves.make_it_sound(frame.columns, frame.rows)
  end
end

task :default => :test

