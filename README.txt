
                                   DES  RÉELS

                                     * * *

                                    SYNOPSIS


                        Protocole de typoésie générative 





D'abord il y a le SOUFFLE, répétition d'un motif de caractères de ponctuations.

Souffle court

  .:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).:).    

ou souffle ample

  ..:;(((..:;(((..:;(((..:;(((..:;(((..:;(((..:;(((..:;(((..:;(((..:;(((..:;((








Puis vient le CŒUR, battement de caractères dans lesquels le souffle peut
s'immiscer, s'échapper, déraper ou aller voir ailleurs. Il est constitué de
plusieurs motifs plus ou moins espacés pour poser un rythme singulier.

  oaVx    d     dX   d     oaVx    d     dX   d     oaVx    d     dX   d     o







Enfin les NERFS, composante immatérielle, sans matière à proprement parler,
forme une trame unique plus ou moins vibrante. Les NERFS naissent du SOUFFLE au
travers des battements du CŒUR pour créer une onde modulée par une tension qui
varie selon l'ouvrage. Grâce à un algorithme qui prélève aléatoirement des
signes du SOUFFLE et du CŒUR, les NERFS mettent en scène et en mouvement ceux-ci
sur un troisième plan. 







Dans cette possible infinité de trames, une sélection sera effectuée pour servir
l'identité de chaque ouvrage de la collection DES RÉELS, en fonction du souffle,
du cœur et des nerfs de chaque livre. Les trois trames produites seront
déployées sur 3 feuilles de couverture.

* La trame des NERFS sera utilisée en fond de page comme marqueur qui soutient
  la composition typographique de la première et quatrième de couverture. 
* Le SOUFFLE s'étale sur la deuxième feuille,
* puis le CŒUR sur la troisième.

L'ensemble forme ainsi une triple couverture, un écrin à effeuiller pour révéler
les mots de l'ouvrage.







                                                  ASCIILANDER, le 29 avril 2021.


