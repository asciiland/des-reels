# -*- encoding: utf-8 -*- 
# -*- frozen_string_literal: true -*- 
# -*- warn_indent: true -*- 

=begin

                               Éditions du commun

                              Collection des réels



                                     BREATH,
                                      HEART
                                      and
                                     NERVES

=end


module EDC
  module Reals

    BREATH_STRINGS = {
      a: ',;).'
    }

    HEART_STRINGS = {
      a: '. oaVx   e/ /    dX   d    '
    }

    TENSE_DEFAULT = 0.5

    module Stretchable
      def stretch_to_line(pattern, columns, rows=1)
        q, m = (columns * rows).divmod(pattern.length)
        (pattern * q) + pattern.slice(0, m)
      end

      def stretch_to_frame(pattern, columns, rows)
        str = stretch_to_line(pattern, columns, rows)
        frame = [] 
        rows.times do |n|
          frame << str.slice!(0, columns)
        end
        frame
      end

      alias_method :stretch, :stretch_to_frame
    end

    class Frame
      include Stretchable

      attr_reader :breath, :heart, :nerves
      attr_accessor :columns, :rows, :tense

      def initialize(breath_string, heart_string, columns, rows, tense = 0.5)
        @breath = Breath.new(breath_string)
        @heart = Heart.new(heart_string)
        @nerves = Nerves.new(@breath, @heart)

        @columns = columns
        @rows = rows
        @tense = EDC::Reals::TENSE_DEFAULT
      end

      def render
        puts "BREATH >>>"
        puts @breath.make_it_sound(@columns, @rows)
        puts "HEART >>>"
        puts @heart.make_it_sound(@columns, @rows)
        puts "NERVES >>>"
        puts @nerves.make_it_sound(@columns, @rows, @tense)
      end
    end

    class Vibration
      include Stretchable

      DEFAULT_SOUND_COLUMNS = 100

      attr_reader :signs

      def initialize(signs)
        @signs = signs
      end

      def make_it_sound(columns=DEFAULT_SOUND_COLUMNS, rows=1)
        stretch_to_line(@signs, columns, rows)
      end

      def to_s
        make_it_sound
      end
    end

    class Breath < Vibration; end

    class Heart < Vibration; end

    class Nerves
      include Stretchable

      attr_reader :breath, :heart

      def initialize(breath, heart)
        @breath = breath
        @heart = heart
      end

      def make_it_sound(columns=DEFAULT_SOUND_COLUMNS, rows=1, tense = 0.5)
        # TODO
        # utiliser tense pour faire varier la densité du mask
        blanks = Array.new(10) do
          Array.new(rand(7..10)) { false }
        end
        fills = Array.new(10 * tense) do
          Array.new(rand(3..6)) { true }
        end

        signs = (blanks + fills).shuffle.flatten
        frame = stretch_to_line(signs, columns, rows)

        breath_frame = @breath.make_it_sound(columns, rows)
        heart_frame = @heart.make_it_sound(columns, rows)

        nerves = Array.new(frame.length) do |n|
          frame[n] ? breath_frame[n] : ' '
        end

        frame.shuffle!
        nerves.map!.with_index do |v, idx|
          frame[idx] ? heart_frame[idx] : v
        end

        nerves * ''
      end

      def to_s
        make_it_sound
      end
    end

  end
end


=begin

TODO
====

* [ ] Rendu SVG: utiliser [Victor](https://github.com/DannyBen/victor)

=end

