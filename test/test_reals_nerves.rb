require "minitest/autorun"
require "../draft.rb"

class TestRealsNerves < Minitest::Test
  def setup
    breath_string = EDC::Reals::BREATH_STRINGS[:a]
    heart_string = EDC::Reals::HEART_STRINGS[:a]
    @frame = EDC::Reals::Frame.new(breath_string, heart_string, 290, 210)
    @nerves = @frame.nerves
  end

  def test_nerves_make_it_sound
    puts @nerves.make_it_sound(290)
  end
end

