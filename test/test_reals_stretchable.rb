require "minitest/autorun"
require "../draft.rb"

class TestRealsStretchable < Minitest::Test
  def setup
    @string = "1234567890" * 811
    @stretchable = Object.include EDC::Reals::Stretchable
  end

  def test_must_stretch_to_line
    assert @stretchable.respond_to? :stretch_to_line
  end

  def test_must_stretch_to_frame
    assert @stretchable.respond_to? :stretch_to_frame
  end

  def test_must_stretch
    assert @stretchable.respond_to? :stretch
    assert_equal :stretch_to_frame, @stretchable.method(:stretch).original_name
  end


  def test_strecth_to_line
  end
  
  def test_stretch_to_frame
    frame = @stretchable.stretch_to_frame(@string, 37, 20)

    assert_equal 20, frame.length
    assert_equal 37, frame.first.length

    frame = @stretchable.stretch_to_frame('01234567', 43, 20)

    assert_equal 20, frame.length
    assert_equal 43, frame.first.length
  end
end

