require "minitest/autorun"
require "../draft.rb"

class TestRealsVibration < Minitest::Test
  def setup
    breath_string = EDC::Reals::BREATH_STRINGS[:a]
    @vibration = EDC::Reals::Vibration.new breath_string
  end

  def test_vibration_when_make_it_sound
    assert_equal (29*21), @vibration.make_it_sound(29,21).length
    assert_equal 100, @vibration.make_it_sound(100).length
  end
end

