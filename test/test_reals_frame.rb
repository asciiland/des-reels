require "minitest/autorun"
require "../draft.rb"

class TestRealsFrame < Minitest::Test
  def setup
    breath_string = EDC::Reals::BREATH_STRINGS[:a]
    heart_string = EDC::Reals::HEART_STRINGS[:a]
    @frame = EDC::Reals::Frame.new(breath_string, heart_string, 290, 210)
    # @frame.render
  end

  def test_vibration_initialization
    assert_equal EDC::Reals::TENSE_DEFAULT, @frame.tense 
  end
end

